---
layout: post
title:  "CS373 Spring 2022: Geethika Hemkumar - Blog 1"
date:   2022-01-22 22:28:00 -0300
categories: jekyll update
---
<p align="center">
    <img src="/cs-373-spring-2022-geethika-hemkumar/assets/images/geethika_pic.png">
</p>

### Where did you grow up?

I was born in Minnesota, where I spent the first few years of my life, and then moved to Austin where I have been ever since! 

### What high school did you attend?

I attended Westlake High School. 

### What was your favorite extracurricular activity in high school?

My favorite extracurricular activity in high school was robotics. I participated in FIRST robotics for two years; I enjoyed getting to write code for a new competition challenge each year. Going to competitions was also exciting, as these events were a culmination of my code-writing efforts, a chance to meet new people from around the city/state, and time to spend with friends.

### Why did you come to UT?

Of the schools I was accepted into for CS, UT felt most like home. My family lives close by so I can spend time with them often, and UTCS is a great program. I've also been coming to UT for piano festivals and summer camps since I was younger and always enjoyed those visits.

### Why are you majoring in CS?

Math has been my favorite subject since elementary school, and since I was younger I knew that I wanted to pursue an engineering-related career. I took AP Computer Science early on in high school and instantly found excitement in learning about programming and its constructs. Programming robots in high school was also an enjoyable experience and helped me to realize that I wanted to major in CS. 

### Why are you in this class?

I have a software engineering internship this summer and thus wanted to take this class as preparation for it. Additionally, I want to learn more about software engineering and what it entails, as well as the tools used and best practices. 

### What are your expectations of this class?

I expect to further hone the skills of self learning, problem solving, and communication. Additionally, I would like to gain a better understanding of tools such as Docker and CI/CD so that I can use these in future personal and work projects.

### How much Javascript/Python/SQL/Web programming do you already know?

I have worked on developing web applications using React in the past, but they were smaller scale projects so I hope that I can build on my understading of React/web development through this class. I have the most experience with Python through previous classes, internships, and research. I have a basic understading of SQL and JavaScript, but have not used them much in the past.

### How did you like the first lectures?

The first lectures were great! I can tell that Professor Downing is excited about what he's teaching and I like how he structures his lectures. The lesson on Docker from Friday was interesting, as I've been using Docker recently and have been wanting to understand some of the fundamentals/its purpose. 

### How did you feel about the cold calling?

Normally I would be nervous about cold calling because I'm naturally a very reserved person, but I think it will keep me engaged. It's nice getting to put names to faces of my classmates as well! I haven't been called yet but I somewhat look forward to it because I think it will be a positive learning experience for me.

### What made you happy this week?

Being back on campus has made me happy! All of my classes this semester seem exciting and I'm looking forward to diving into more material. I also started working on my research again this week, which kept me busy between classes. It's nice to get back into a routine again after a much needed break.

### What's your pick-of-the-week or tip-of-the-week?

My tip-of-the-week is to document how you fixed a bug thoroughly (e.g., what additional packages you needed to install or terminal commands you ran). This will help reduce time (and frustration!) spent in the future if you or someone else runs into a similar issue. 

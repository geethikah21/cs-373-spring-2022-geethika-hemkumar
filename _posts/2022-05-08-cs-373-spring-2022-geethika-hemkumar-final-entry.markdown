---
layout: post
title:  "CS373 Spring 2022: Geethika Hemkumar: Final Entry"
date:   2022-05-08 14:55:00 -0300
categories: jekyll update
---
<p align="center">
    <img src="/cs-373-spring-2022-geethika-hemkumar/assets/images/geethika_pic.png">
</p>

- Test first, test during, test after; test, test, test
- When designing algorithms, demand the weakest capabilities (e.g., iterable vs. indexable)
- When designing containers, provide the strongest capabilities (e.g., indexable vs. iterable)
- Build decorators on top of containers, iterators, and functions
- Utilize the benefits of being lazy (e.g., yield)
- Always look for reuse and symmetry in your code
- Collaboration is essential to the quality of your code and to your well-being in producing it
- Refactor, refactor, refactor
- Make your code beautiful

### How well do you think the course conveyed those takeaways?

Overall, I think the course conveyed the above takeaways well. The last three takeaways resonate with me the most. Throughout the project, I tried to constantly think about ways to refactor my code to make it prettier and easier to understand. I associate the other takeaways with the concepts we learned from lectures. I learned a lot about features of Python I hadn't known about before. 

### Were there any other particular takeaways for you?

Through the project, I learned the importance of completing tasks promptly, especially when others depend on the completion of work you're responsible for. I definitely could have completed the work I promised earlier so others could complete theirs efficiently and effectively as well. 

### How did you feel about cold calling?

I was initially worried about cold calling because I thought I would be expected to know the answer to all the questions I was asked, but my experience of cold calling was quite the opposite. Professor Downing uses cold calling as a learning experience and a way to encourage paying attention in class. I appreciated cold calling because being called on gave me a chance to contribute to the class discussion. 

### How did you feel about specifications grading?

I thought that the specifications grading system was good for all assignments except the projects. Each project has so many requirements, and while I don't mind the application of the EMRN scale to each requirement, I don't think that an entire project should be determined by the lowest letter given out of all the requirements. I think that each project's grade should be determined by the average (or majority) of the letters given for each sub-section of the project, similar to how each quiz's grade is determined (average of score for both stages of the quiz). 

### How did you feel about help sessions and office hours?

I went to a few help sessions/office hours. They were generally helpful, as I received suggestions on how to debug the issues I was facing, but I relied mostly on online searches to resolve bugs in my code/understand concepts taught in class better.

### How did you feel about the support from the TAs?

I didn't get a chance to interact much with the TAs other than my assigned mentor TA (Anshul), but the TAs answered my questions to the best of their abilities, which was much appreciated. Weekly project meetings with Anshul were helpful, as he was able to answer my/my group's project questions or point us in the right direction regarding where we could look to figure out the answers to our questions. 

### You should have read five papers that describe SOLID design: Single Responsibility, Open-Closed Principle, Liskov Substitution, Interface Segregation, and Dependency Inversion. What insights have they given you?

After reading the papers about SOLID design, I feel that I have a better understanding of object-oriented programming and good object-oriented design practices. A significant idea I took away from reading these papers is to utilize abstraction more often to facilitate the extension of software and reduce rigidity. 

### You should have read two papers that advised minimizing getters and setters. What insights have they given you?

I previously thought that getters and setters were a crucial part of every object, but after reading the papers in the class, I've learned that getters and setters unnecessarily reveal implementation details of a class. This increases the fragility of code that depends on these objects. Code that relies on the operation of a class's instance variables should be written in methods within the class itself when possible to reduce the revelation of implementation details outside of the class. 

### What required tool did you not know and now find very useful?

Flask was a useful tool I learned about through this class. I've always been curious about how the backend of a website is developed. Learning about and using Flask in the web project this semester has helped me better understand how backend development works. 

### What's the most helpful Web dev tool that your group used that was not required?

For the backend, [Marshmallow](https://marshmallow.readthedocs.io/en/latest/) was the most helpful non-required Web dev tool. Marshmallow integrates well with SQLAlchemy. The [marshmallow-sqlalchemy](https://marshmallow-sqlalchemy.readthedocs.io/en/latest/) library allowed us to easily convert a SQLAlchemy model instance, retrieved from a SQLAlchemy query to our database, to a Python object that could be jsonified and returned by our API.

### How did you feel about your group having to self-teach many, many technologies?

I believe that self-teaching is an important skill to develop, especially in computer science/software engineering. I think having to teach myself/learn about the technologies from others in my group was a valuable experience and gave me a sense of what development may be like in industry. I do think learning about best practices for frontend/backend development in lectures would have been useful, however, as I'm relatively new to web development. 

### In the end, how much did you learn relative to other UT CS classes?

I learned a lot from this class, but in different ways than how I've learned in other UT CS classes. More specifically, I've strengthened my ability to write through the technical reports, search online for ways to debug my code and learn new technologies quickly. I've primarily "learned by doing" in this class, while in other UT CS classes I've learned by reading from a textbook and doing assignments (the more traditional way). Compared to other UT CS classes, I think I've learned about the same amount of information. This class gave me a great introduction to software engineering that has given me the foundation to explore the field further.

### Give me your suggestions for improving the course, but apologies in advance; specifications grading will remain.

The papers are certainly interesting, but adding 7 meaningful annotations to each paper on Perusall was tedious at times. I think that writing a 100-200 word response after reading each paper and writing a few questions for others to answer would be more useful. Additionally, I think expanding lecture content to include an introduction to best practices regarding code style/design would be helpful. Overall, I enjoyed taking this course, and gained many hard/soft skills that will continue to be useful to me! 
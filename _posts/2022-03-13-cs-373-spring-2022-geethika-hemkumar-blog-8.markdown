---
layout: post
title:  "CS373 Spring 2022: Geethika Hemkumar - Blog 8"
date:   2022-03-13 20:33:00 -0300
categories: jekyll update
---
<p align="center">
    <img src="/cs-373-spring-2022-geethika-hemkumar/assets/images/geethika_pic.png">
</p>

### What did you do this past week?

I'd say this past week was probably one of the busiest weeks I've had while in college. I helped run a research study with human participants for a project I'm part of, so I was in the robotics lab for quite some time this week. I also took an exam and turned in an assignment for my data mining class and submitted my first essay for my literature class. Finally, I attended a meeting for the UAV organization I'm part of; the hardware and software teams have made great progress on our project and I'm excited to see what we can accomplish by the end of the semester.

### What's in your way?

I've fallen behind on a lot of schoolwork the past few weeks, so I'll likely be spending a significant amount of time catching up over spring break. Hopefully, I can find the time to catch up and get ahead on assignments during the break and help my group get through a significant portion of phase 2 so that we're not cramming too much of it the week it's due.

### What will you do next week?

Next week, I'll be catching up on schoolwork and working on submitting a graduate school application that's due the week we get back from break. I also plan to spend quality time with family and hope to hang out with friends.

### What did you think of Paper #8: Interface Segregation Principle?

I had to read this paper a few times to grasp its major points, but after doing so I can say that I've learned a lot from it. I enjoyed learning about the ATM system and the object-oriented design associated with it. This example highlighted the ISP very well, ultimately showing that the best design was to map each transaction class derivative to a corresponding UI class derivative to separate functionality and ensure that each interface only depended on the functionality it needed. I also thought the explanation of multiple inheritance in the context of the ISP was informative. 

### What was your experience of comprehensions and yield?

I've heard of comprehensions and `yield` and have seen it used in code before, but I wasn't entirely sure of its purpose until now. Regarding comprehensions, I'm most intrigued by how wrapping a comprehension in parentheses creates a generator, while using square or curly brackets creates a data structure (list, set, dict, etc) with the results of the comprehension. Learning about `yield` was also quite interesting. Completing the digits iterator exercise using `yield` significantly helped my understanding.

### What made you happy this week?

I had a lot of fun running the research study for my robotics project this past week. Though we encountered many issues along the way, seeing and putting the study into action was exciting because it was the culmination of what my group and I had been working towards all year. I also got to FaceTime a friend whom I haven't called in a while and we've made plans to hang out over break which I'm quite excited about. Finally, I was happy to come home yesterday evening and see my family. My brother (who is 8 years younger than me) is my height now, which is astonishing to me.

### What's your pick-of-the-week or tip-of-the-week?

My pick-of-the-week is this [JSON library](https://github.com/nlohmann/json)! It can be used to serialize/deserialize data to/from a JSON object in C++. I previously used it to standardize the format of data being sent over a socket. It's easy to integrate into your code, as you just have to include a single hpp file. Usage is also simple, and the GitHub repository includes a plethora of examples. 
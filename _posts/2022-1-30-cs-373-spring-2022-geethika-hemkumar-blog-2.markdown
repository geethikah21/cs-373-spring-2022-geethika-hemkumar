---
layout: post
title:  "CS373 Spring 2022: Geethika Hemkumar - Blog 2"
date:   2022-01-30 12:31:00 -0300
categories: jekyll update
---
<p align="center">
    <img src="/cs-373-spring-2022-geethika-hemkumar/assets/images/geethika_pic.png">
</p>

### What did you do this past week?

This past week, I was getting into the routine of my class schedule and trying to stay on top of assignments. I held a couple of office hours for the class I'm a TA for this semester; the students seem nice so far. Almost every day after my classes finished, I went to my research lab and worked for a few hours. My group and I made some good progress on our research project this week, which I'm happy about. 

### What's in your way?

There's one last hurdle to get over for my research project, which has been frustrating because I'm not quite sure how to solve it. 
Collatz has been a little overwhelming because there are several components and the project involves many libraries I haven't used before, but I'm slowly starting to become more comfortable with the workflow.  

### What will you do next week?

My activities next week will be similar to those of this past week. I'll mostly be attending classes, finishing up homework, and working on my research project. I'll have to get used to a new routine, as some of my classes will be in person and I'll have to hold office hours in person next week as well. I hope to be finished with the majority of Collatz by the end of next week. 

### What did you think of Paper #1: Syllabus?

I liked that the syllabus thoroughly laid out the class structure and how our grades will be computed. I also appreciated that the details/expectations of each assignment category were clearly explained. Annotating the syllabus helped me to gain a better understanding of its content. Determining what to annotate on Perusall was a little difficult though, as much of it was understandable on its own. 

### What was your experience of assertions and Collatz? (this question will vary, week to week)

I've used assertions in the past during my internship last summer as well as in an organization I'm in. Learning about where assertions should/shouldn't be used in a program was still very useful. Regarding Collatz, I started setting up my GitLab environment by adding milestones and setting deadlines to complete these milestones by. I hope to make more progress on Collatz in the coming days. My goal is to finish the functionality portion of Collatz (the base code and necessary optimizations) by the middle of this week.

### What made you happy this week?

I had Pizza Press for dinner on Friday (and also **just** realized the meaning of Pizza Press's name). I also went to the mall with my mom on Saturday and bought jewelry!   

### What's your pick-of-the-week or tip-of-the-week?

My pick-of-the-week is [oh-my-bash](https://github.com/ohmybash/oh-my-bash)! It's a framework that allows you to add tools such as themes and plugins to your bash configuration. I like that the themes give the text on my terminal colors other than white. The themes also display useful information about Git repositories on my local machine, notably what branch I'm on when my current directory is a Git repository. I have yet to explore oh-my-bash further, but I've noticed that there's a Git plugin available that enables aliasing for many common Git commands which could be useful.  
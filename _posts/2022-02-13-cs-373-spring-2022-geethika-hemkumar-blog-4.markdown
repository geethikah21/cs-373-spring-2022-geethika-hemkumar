---
layout: post
title:  "CS373 Spring 2022: Geethika Hemkumar - Blog 4"
date:   2022-02-13 15:38:00 -0300
categories: jekyll update
---
<p align="center">
    <img src="/cs-373-spring-2022-geethika-hemkumar/assets/images/geethika_pic.png">
</p>

### What did you do this past week?

This past week, I was mostly busy with attending classes and finishing up assignments. Collatz kept me busy on Monday and Tuesday, as I was working on formatting and testing my code and making sure that I met all of the requirements. After Tuesday, I spent more time on my research project - we were able to finish up the software portion of the project and show it to faculty members, which was exciting! Now we just have to make some final changes to the software and then we can begin the second portion of the project. I also had quite a busy office hour on Friday, as the students had a homework assignment due that night.  

### What's in your way?

I've fallen behind on my English literature assignments, as they can be time-consuming and I'm not a very fast reader/writer. I hope to catch up on them in the coming days now that I've turned in some larger projects from other classes. 

### What will you do next week?

Next week, I will work on putting the final touches on the software for my research project. I'll also work on the next data mining assignment and start the next project for this class if it gets released. I'm planning on applying to a couple of graduate school programs this semester, so I'm also hoping that I'll get to spend time on these applications and further explore other programs I may apply to next semester. 

### What did you think of Paper #4: Pair Programming?

I've had a good amount of experience with pair programming through data structures and OS and read this paper in both classes, but it was nice to get a refresher on the ideas presented in the paper. I've certainly learned from this paper and strive to apply all of the mentioned pair programming principles. One that resonates with me is "share everything," as it's very important to have a thorough understanding of all code that the pair writes. All of my programming-related classes this semester involve group work, so I'll be sure to continue applying the lessons from the paper.

### What was your experience of exceptions and types?

I've used exceptions in projects for previous classes, but haven't learned about them in detail until this class. I previously thought that the "else" clause of a try-except block only runs if none of the "except" clauses run, but from class, I learned that any raised exception implies that the else clause does not run, regardless of whether or not the exception is handled. 

I've had a similar experience with types. I never completely understood the difference between the "is" keyword and == until last week's class. Also, learning the fact that ints are considered values within [-5, 256] and addresses outside of this range was pretty interesting to me.

### What made you happy this week?

The weather has been pretty nice recently (except yesterday, when it was cold and rainy)! Walking outside and seeing clear blue skies from my apartment window has been nice. I also got to see a good number of friends who I haven't seen in person in a while, which was nice. 

### What's your pick-of-the-week or tip-of-the-week?

My pick-of-the-week is a tool called [whenisgood](https://whenisgood.net/)! It's a tool that makes scheduling meetings with multiple attendees easier. One person just needs to set up an event with all possible dates/times for the meeting, and whenisgood will create a link that they can send to everyone else. All attendees just need to select the times that they are available. Whenisgood will keep track of which times everyone is free on a separate results page, which will graphically show when most/all people are free. I've used it quite a bit recently and it's been incredibly helpful. 
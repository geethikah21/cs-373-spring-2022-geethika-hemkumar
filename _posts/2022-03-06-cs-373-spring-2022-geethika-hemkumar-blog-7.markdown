---
layout: post
title:  "CS373 Spring 2022: Geethika Hemkumar - Blog 7"
date:   2022-03-06 21:53:00 -0300
categories: jekyll update
---
<p align="center">
    <img src="/cs-373-spring-2022-geethika-hemkumar/assets/images/geethika_pic.png">
</p>

### What did you do this past week?

This past week was busy, but I enjoyed it. I attended class and caught up on several assignments. I was in my research lab for several hours this week, as we're pushing to submit a paper in a couple of weeks. The weather towards the end of the week was beautiful, so I spent quite a bit of time outside this week as well.

### What's in your way?

Between classes and my extracurriculars, next week will be busy for me. I'll need to very carefully manage my time next week so that I'll have time to accomplish what I need to in a day. This upcoming week will be a challenge, but I'm excited to see how it goes. Regarding phase 2, one of the APIs my team was planning on using to scrape data from is not longer functional. We need to come up with a new model to replace one of our current ones, and find another API to supply data for this new model as well.

### What will you do next week?

Next week, I'll hopefully be wrapping up my current research project - we'll be running a study that requires participants which is exciting, but also nerve-wracking because there's a lot to keep track of in the process. Outside of that, I'll be keeping up with assignments/classes I have during the week. I especially hope to dedicate significant time to making progress on phase 2, as my group is hoping to complete a good amount of this project before spring break.

### What did you think of Paper #7: Liskov Substitution Principle?

I agree with many others who have said that this paper was difficult to understand at first. I had to read it a few times to more thoroughly understand its content. The PersistentSet example was probably the most complicated for me to understand. I've never learned about object-oriented design in this much depth before, so learning about the LSP-compliant solution was initially mind-boggling but makes more sense now. Overall, I learned more about object-oriented design by reading this paper, which I appreciated.

### What was your experience of operators and iteration?

Learning about how the "Pythonic" parts of Python work has been interesting. I didn't know that the right-hand side of the += operator could be any iterable when operating on a list. I was also unaware of how class variables worked in Python before discussing this in lecture. Regarding iteration, I was intrigued that calling iter() on an iterator would return the iterator object itself. Learning about how this feature could be used was fascinating and I'll be sure to use it in the future when necessary. Overall, learning more about the workings of Python has been exciting and I've gained a better understanding of Python.

### What made you happy this week?

I played Super Smash Bros with some friends this weekend! I haven't played in a while (and I'm not that good), but regardless I had fun spamming the keys on a controller for some time :). This was a nice way to unwind after a week of hard work. I'm also excited that spring break is coming up and that the weather seems to slowly be getting warmer.

### What's your pick-of-the-week or tip-of-the-week?

My pick-of-the-week is this [Swift wrapper of SQLite3](https://github.com/stephencelis/SQLite.swift)! I found this when I started creating an iOS app for a personal project and it's been easy to use thus far. This library adds a Swift interface to the SQLite C API, which eases the integration of a SQLite database into a Swift-based iOS application.
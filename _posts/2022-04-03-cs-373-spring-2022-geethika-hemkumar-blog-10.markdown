---
layout: post
title:  "CS373 Spring 2022: Geethika Hemkumar - Blog 10"
date:   2022-04-03 20:30:00 -0300
categories: jekyll update
---
<p align="center">
    <img src="/cs-373-spring-2022-geethika-hemkumar/assets/images/geethika_pic.png">
</p>

### What did you do this past week?

For me, this past week was less busy than the previous few. I spent a large part of the week working on phase 2. My group was able to make significant progress on our website, but we've also found many aspects that we need to improve on for phase 3, particularly regarding the format of the data that is persisted in our database. Outside of working on the SWE project, I attended class and held office hours per usual. I also finally finished up a literature assignment, which I spent many nights and days working on.

### What's in your way?

Other than time, not much is in my way. I have several midterms and project deadlines coming up, along with a few non-school-related deadlines, so I'll need to manage my time well. I also would like to make time to work on personal projects and start reading books again, which may be difficult (but doable) given the end-of-semester busyness.

### What will you do next week?

Next week, I will be studying for a business law midterm I have on Wednesday and making progress on/submitting various assignments I have in my classes. I have a data mining midterm in a couple of weeks, so I will start studying for that as well. Outside of school assignments, I will be making progress on a research paper which is due in a few weeks, and hopefully contributing to the UAV organization I'm an officer of by helping the software subteam make progress on our project. 

### What did you think of Paper 10: Why getter and setter methods are evil?

This paper built on and strengthened my understanding of object-oriented programming. The idea that accessor methods revealed implementation details of a class didn't register until reading this paper, but the concept certainly makes sense. Now I realize that true object-oriented programming involves implementing functions within a class to operate on its variables instead of using accessor methods to retrieve and use member variables outside of the class. Also, learning about the CRC (class, responsibilities, collaborators) method was interesting and I'll try to make use of this framework when planning out classes and their relationships in the future. 

### What was your experience of functions, regular expressions, and relational algebra?

Learning about dict and tuple packing was quite fascinating, and the last quiz question from Wednesday's quiz improved my understanding of these concepts. I'm most intrigued by the fact that Python will pack named arguments into a dictionary if dict packing is present, even if a named argument passed into the function isn't specifically noted in the function signature. 

I've seen and learned briefly about regular expressions in my computer science club in high school, but I've never fully understood how to make use of them. After learning about them in class, I believe that I've developed a basic understanding of regular expressions. I can use this knowledge to learn more about this topic. 

Learning about relational algebra on Friday was interesting as well! I've never thought about how SQL is implemented, so I'm excited to see where the discussion about implementing SELECT with a list of dictionaries and unary functions goes.

### What made you happy this week?

Spring is my favorite season, so I've enjoyed the weather this past week. I also set up my projector last (Saturday) night and finally watched the first episode of _Moon Knight_, which was exciting. The first episode dove right into the action without revealing details about what was going on (typical of Marvel), but I think the show is off to a good start and I'm excited to see how the plot develops.

### What's your pick-of-the-week or tip-of-the-week?

My tip-of-the-week is to have a USB-A to USB-C (and vice versa) converter handy. The USB-C connector seems to be getting more popular (e.g., with mobile devices and laptops), but many devices/cables are still using USB-A. Being able to convert between the two connector types is thus useful. 
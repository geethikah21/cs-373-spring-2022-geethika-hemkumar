---
layout: post
title:  "CS373 Spring 2022: Geethika Hemkumar - Blog 12"
date:   2022-04-17 22:00:00 -0300
categories: jekyll update
---
<p align="center">
    <img src="/cs-373-spring-2022-geethika-hemkumar/assets/images/geethika_pic.png">
</p>

### What did you do this past week?

This past week, I studied for and took a midterm for Data Mining at the beginning of the week. I spent a good amount of time this week working on the backend for phase 3. I'm glad that my group was able to finish phase 3 on time and complete a significant chunk of work on our website. The deadline for submitting research papers to a workshop that my group was aiming to submit our work to was this past Saturday, so I spent significant time on Saturday morning helping to put the final touches on the paper. We found some significant results from our experiment, which was exciting.

### What's in your way?

I have quite a few upcoming assignment/project deadlines, so I'll need to spend a significant chunk of time working on those this week.

### What will you do next week?

Next week, I'll be attending class and working on the aforementioned projects and assignments. I hope to coordinate with my group and start on phase 4 so that we'll have time to complete all of this phase's requirements and incorporate feedback from phase 3. Also, I have a big presentation on Friday which I'll need to spend a little bit of time preparing for. 

### What did you think of Paper 12: Why extends is evil?

I thought this paper was easier to understand compared to the previous ones, likely because the concepts discussed in this paper built on those from previous ones. The example about how the change to the `push_many` method in the `Stack` class broke the `push_many` functionality in the `Monitorable_stack` class was particularly informative to my understanding of why implementation inheritance should be avoided when possible. 

### What was your experience of natural join and SQL?

I have basic familiarity with SQL through previous classes and projects. However, I'm not familiar with SQL joins, so completing the natural join exercise in class was useful to my understanding of the operation. The SQL Hackerrank exercise was also useful and enjoyable. I didn't know that the `between` operator existed in SQL. 

### What made you happy this week?

I got to come home for part of this weekend! I enjoyed spending time with my brother and parents. I also got to work on a personal project and was excited to be able to make progress on some of its functionality. 

### What's your pick-of-the-week or tip-of-the-week?

My tip-of-the-week is that the best way to learn a piece of software or programming language is to do a project! I believe that learning by doing is one of the best ways to quickly gain familiarity with anything. Being able to associate tangible examples with the usage of a software library or programming/CS concept improves your understanding significantly.
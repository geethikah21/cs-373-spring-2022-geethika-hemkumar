---
layout: post
title:  "CS373 Spring 2022: Geethika Hemkumar - Blog 11"
date:   2022-04-10 22:04:00 -0300
categories: jekyll update
---
<p align="center">
    <img src="/cs-373-spring-2022-geethika-hemkumar/assets/images/geethika_pic.png">
</p>


### What did you do this past week?

This past week was busy but manageable. I turned in a literature assignment on Tuesday and studied for my business law midterm on Monday and Tuesday. I had my exam on Wednesday afternoon - I did well enough. After that, I met with my group a few times to discuss the revisions we wanted to make after phase 2 and plan for phase 3. Outside of school, I was finally free to meet with the software lead of the UAV organization I'm part of. We were able to make some progress, which I'm happy about. 

### What's in your way?

Similar to previous weeks, time is the main thing in my way. This week will be especially busy for me with an exam on Tuesday and phase 3 due on Thursday. I additionally have homework due for another class on Friday that I expect will take some time, and I need to catch up on my literature assignments. 

### What will you do next week?

During the former half of next week, I will study for and take my data mining exam. I hope to continue making progress on phase 3 throughout the week so that we can turn it in on time. Meetings for the directed reading program (DiRP) within the computer science department have started to pick up, so I hope to spend some time studying this week's readings before my meeting.

### What did you think of Paper 11: More getters and setters?

I thought this paper was a great sequel to last week's. The UI example was interesting, as it conveyed the author's main idea of reducing reliance on getter/setter methods. The author's final point about the benefits of moving away from "procedural thinking" when utilizing object-oriented programming resonated with me. I've previously used classes solely as a collection of instance variables and getter/setter methods and wrote functions outside of the class to operate on the object's variables, but I now realize that moving these functions to within the class is a better design. I also appreciated that the author pointed out that though it's not possible to eliminate the use of accessor methods, the maintainability of software can be improved by limiting the scope of use of getter/setter methods.

### What was your experience of select, project, and cross join?

I've learned/used basic SQL in the past, so I was previously familiar with the select query. Nevertheless, implementing `select` using a dictionary as a relation was useful to my understanding of the query. The `project` and `cross join` queries were new to me, but I feel that I understand their purposes after implementing them in Python using dictionaries and iterators. 

### What made you happy this week?

I got a chance to catch up with some old friends this afternoon! It was nice to get lunch with them and hear about what they've been up to lately. I was also able to call my parents and brother this evening; even if we're not talking the whole time, it's nice to just be on the phone with them while we simultaneously finish the things we need to do.  

### What's your pick-of-the-week or tip-of-the-week?

My tip-of-the-week is to utilize Python [virtual environments](https://docs.python.org/3/library/venv.html#module-venv)! These allow you to potentially use a different version of Python than what's installed on globally on your computer. Additionally, you can install Python packages within this environment without impacting your computer's global package installation. My group has been using a virtual environment to keep track of package requirements for the backend of our website. I also recently used it while downloading software for my organization, as I wanted to reduce the risk of potentially breaking my global package configuration.
---
layout: post
title:  "CS373 Spring 2022: Geethika Hemkumar - Blog 13"
date:   2022-04-24 23:04:00 -0300
categories: jekyll update
---
<p align="center">
    <img src="/cs-373-spring-2022-geethika-hemkumar/assets/images/geethika_pic.png">
</p>

### What did you do this past week?

At the beginning of the week, I spent quite a bit of time working on my poster for a research presentation I had this past Friday. I also worked a lot on my data mining project and a literature project I had due this week. Outside of attending class and completing homework, I registered for classes and met with my group to discuss our plan and a rough timeline for completing phase 4. I also met with the software team of the UAV organization I'm part of to try to get a UAV simulator up and running. 

### What's in your way?

This past week was filled with assignment deadlines, and the next few will be as well. I hope I can find the time to accomplish everything that I want to this week.

### What will you do next week?

I will finish up my data mining project tomorrow and start a literature assignment due on Thursday. I also hope to spend some time further exploring what I want to do post-graduation, as this is something that I've had limited time to do this semester. 

### What did you think of Paper 13: What Happens to Us Does Not Happen to Most of You?

I'm aware that sexism does occur in the workplace, especially in fields such as CS, but I was still shocked to learn of the degree to which sexism impacted the lives of the women in CS who shared anecdotes in the paper. Raising awareness of sexism in the workplace is important, and I'm glad that articles like these are written and we get a chance to read and learn from them. 

### What was your experience of subqueries and refactoring?

I've learned about subqueries in the past, but never fully understood how to use them until this class. The exercise we completed last week helped me to better understand subqueries. Professor Downing's analogy of the `in` SQL clause to how it's used in Python was also quite helpful. 

Refactoring is probably one of my favorite activities in the process of writing software. The first task in my internship last summer was to refactor part of the codebase for the project I was working on. This task helped me to quickly gain familiarity with the codebase. 

Refactoring parts of the movie/rental/customer classes during Friday's lecture was interesting and opened my eyes to some things to look out for when refactoring. Being able to apply some of the concepts we've learned in recent papers, such as reducing the use of getters/setters as much as possible, was exciting. The concepts I learned from the papers helped me to understand that the reason why we moved the computation of points and rental amount from the Customer class to the Rental class was because the computation of the rental amount and points depended on variables within the Rental class. 

### What made you happy this week?

Mainly, I think I'm happy that I made it through the week. This past week was quite busy and I'm glad that I finally got a chance to relax today. Throughout the week, I think the little things made me happy, such as the weather and the fact that the sun doesn't set until around 8 pm. 

### What's your pick-of-the-week or tip-of-the-week?

My tip-of-the-week is to make sure that you take breaks throughout the day! Setting aside time to decompress each day is extremely important in order to reduce the chance of burnout. Taking breaks also improves your ability to learn and produce quality work.
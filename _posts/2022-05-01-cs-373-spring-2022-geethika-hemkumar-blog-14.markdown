---
layout: post
title:  "CS373 Spring 2022: Geethika Hemkumar - Blog 14"
date:   2022-05-01 21:05:00 -0300
categories: jekyll update
---
<p align="center">
    <img src="/cs-373-spring-2022-geethika-hemkumar/assets/images/geethika_pic.png">
</p>

### What did you do this past week?

I finished up my data mining project on Monday night and met with my group to discuss and work on the provider visualizations for our website. I also worked quite a bit on a literature assignment due this past week and coordinated meeting times with my business law project group. Outside of class, I co-hosted the final meeting of the year for the UAV organization I'm part of.

### What's in your way?

I have quite a bit to do school-wise this upcoming week in all my classes. The last week of classes is always busy, so the workload is expected. I'll need to manage my time well this week. 

### What will you do next week?

Next week, I'll mainly work on finishing the final assignments for all of my classes and hopefully start studying for the third data mining exam and my business law final. I'll also meet with my research group to coordinate any work that needs to be done to hand off work to students working on our project over the summer and try to help students in the class I'm a TA for with their final projects. 

### What did you think of Paper #14: The New Methodology?

I thought it was a good read! I've heard of the Agile methodology previously, but haven't formally learned about it. This paper was a great introduction to the Agile methodology and the motivation behind its inception. I particularly enjoyed the comparison the author made between the design/construction process for software engineering and other engineering fields. I agree with the author's statement that in other engineering fields, the design process mainly finishes before construction, as math can be used to check that the design is complete before constructions start. On the other hand, with software engineering, design and construction happen simultaneously so a more specific methodology is needed. 

### What was your experience of refactoring?

Working through the various refactoring tasks suggested by Fowler in class was fun. The refactoring exercise we did in class on Wednesday, where we transformed the price code integer into an abstract price class and created child classes corresponding to each movie type, was a nice application of the open-closed principle. Learning about how object-oriented principles including inheritance and encapsulation can be used in real-world software has been eye-opening. Before learning about refactoring in class, I've only learned about inheritance/polymorphism/encapsulation but have seen few examples of them used in practice. Now, I have a better sense of how to put these principles into practice and will apply what I've learned to future programming endeavors. 

### What made you happy this week?

My research group recently submitted a paper to a workshop that's part of a big robotics conference, and we just got notified of its acceptance this past week! I'll be going to the conference to present the work we did at the end of May. I'm excited to attend the conference and finally get to travel again. 

I also got to catch up with some friends who I haven't seen/spoken to extensively in a while, which was nice. 

### What's your pick-of-the-week or tip-of-the-week?

I started learning C# recently for my internship this summer and was recommended this [tutorial](https://docs.microsoft.com/en-us/dotnet/csharp/tour-of-csharp/tutorials/) from Microsoft. There are both video and interactive tutorials that provide a basic understanding of the syntax for variable declaration, loops, conditionals, etc. 
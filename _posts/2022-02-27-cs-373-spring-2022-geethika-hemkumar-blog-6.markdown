---
layout: post
title:  "CS373 Spring 2022: Geethika Hemkumar - Blog 6"
date:   2022-02-27 17:15:00 -0300
categories: jekyll update
---
<p align="center">
    <img src="/cs-373-spring-2022-geethika-hemkumar/assets/images/geethika_pic.png">
</p>

### What did you do this past week?

This past week was quite busy for me school and extracurricular-wise. I met with my group a few times this week to both brainstorm ideas for our website and start implementation. Additionally, I had a midterm for my business law class and finished up my data mining assignment. My partner and I were stuck on one bug for some time but were excited when we finally figured it out and submitted the homework. I also had to walk to my research lab many times this past week to work on my project. Walking in the cold was unpleasant at times, but I enjoy the chances I get to go outside. 

### What's in your way?

Because I spent a lot of time working on my research project, class assignments, and studying for my midterm last week, I have yet to start many of the assignments due this upcoming week. The submission deadline for Phase 1 is also coming up, and I hope that I'll be able to implement the features I've agreed to work on well. 

### What will you do next week?

My activities next week will be very similar to this last one. I'll likely be spending a good amount of time finishing up Phase 1 until Monday or Tuesday night. I'll also be working on an essay due for my literature class and another written assignment due for business law. Along with this, hopefully, I'll be helping with the start of the second part of my research project by the end of this week. 

### What did you think of Paper #6: Open-Closed Principle?

I enjoyed reading this paper, as it built on my understanding of object-oriented design. I've completed projects in a few courses that involved implementing classes based on an abstract class. This paper certainly provided more clarity about the advantages of this. My favorite aspect of the paper was that explanation of concepts were built around the Shape abstract class example and accompanying C++ code. For me, a significant takeaway from the paper is to make use of abstract classes to guarantee that certain portions of existing code are closed to modification.

### What was your experience of for in, reduce, object models, and operators?

I've had experience with using the `for in` construct in Python. Nevertheless, the discussion in lecture about its relation to Python iterators was informative as I've never formally learned about them. After learning about `reduce` in class, I have a better understanding of this function and will certainly make use of it in the future when necessary. The discussion about object models made me realize that Python objects are a lot more flexible than those in Java. Finally, the discussion about operators and `__<name>__()` functions that could be used in place of more commonly used operator syntax was also very eye-opening.

### What made you happy this week?

My sleep schedule was inconsistent at the beginning of the week due to assignments and a midterm. Thus, I was happy to get more consistent sleep (plus naps) from Thursday onwards. Now, I feel well-rested and ready to take on the week ahead. Also, today I figured out a problem I've been stuck on since this past Tuesday which was relieving. 

### What's your pick-of-the-week or tip-of-the-week?

My pick-of-the-week is this VSCode extension called [vscode-position](https://marketplace.visualstudio.com/items?itemName=jtr.vscode-position)! The extension allows you to easily determine and access a numbered position within a file. When working on my data mining assignment this past week, my partner and I encountered Git merge conflicts with a Jupyter notebook file. This added unexpected characters to the JSON of the notebook, so we were unable to open it using Jupyter. The error messages included a position (in the file) where formatting errors were, so I used this extension to jump to these positions in the JSON file and fix the errors.

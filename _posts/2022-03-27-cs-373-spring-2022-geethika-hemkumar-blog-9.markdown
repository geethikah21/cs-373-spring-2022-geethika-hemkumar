---
layout: post
title:  "CS373 Spring 2022: Geethika Hemkumar - Blog 9"
date:   2022-03-27 23:42:00 -0300
categories: jekyll update
---
<p align="center">
    <img src="/cs-373-spring-2022-geethika-hemkumar/assets/images/geethika_pic.png">
</p>

### What did you do this past week?

This past week was once again quite busy for me. I submitted an application at the beginning of the week and worked on my research project, attended classes, and made progress on assignments throughout the week. Although the week was busy, I was happy to be back on campus and attend classes again.  

### What's in your way?

The final weeks of the semester will certainly be filled with projects, midterms, and assignments. Dedicating a sufficient amount of time to all of my classes will be crucial. Sometimes, I tend to spend too much time on an assignment. Understanding when further work on an assignment can be better spent will be crucial to my productivity.

### What will you do next week?

Next week, I plan to dedicate a significant amount of time to finishing phase 2 of the project. I also hope to catch up on work for my literature class, start my data mining assignment, and begin studying for my upcoming data mining and business law midterms. Hopefully, the weather from this weekend is maintained next week, as I hope to study outside when I can. Outside of school, the UAV organization I'm part of has planned a social at Zilker on Friday evening. I'm looking forward to spending this time decompressing after what I'm sure will be a busy week. 

### What did you think of Paper #9: Dependency Inversion Principle?

This paper was a great application and synthesis of the Open-Closed Principle and the Liskov Substitution Principle. I appreciated that the examples transitioned from simple to complex as the paper progressed. I learned that employing abstractions can prevent higher-level implementations from being dependent on those in lower levels, thus permitting the overall application to be more easily maintainable as new features are added. 

### What was your experience of decorators, Cache, and functions?

I've seen decorators used in Python code in the past, but I wasn't aware of their purpose and mechanics until learning about them in lecture. I now understand the purpose of the `@app.route()` decorator when defining API endpoints and hence have a better understanding of how a Flask application works. 

The Cache exercise helped further solidify my understanding of decorators. Walking through the code flow with my team after finishing the exercise was extremely helpful to my understanding of how the `@cache` decorator effectively converted the Fibonacci function into an instance of the cache class. 

In class, I learned concepts related to Python functions I hadn't before, particularly that all non-default arguments must come before default ones.

### What made you happy this week?

My birthday was this past Monday! I enjoyed celebrating with cake and spending time with family. I also got to watch the most recent Spider-Man movie at the union, which I have been wanting to watch ever since its release. 

### What's your pick-of-the-week or tip-of-the-week?

My tip-of-the-week is to plan out code before you write it. Doing so allows you to notice and address potential issues with your design before starting implementation, which saves time and reduces the chance of bugs.
---
layout: post
title:  "CS373 Spring 2022: Geethika Hemkumar - Blog 3"
date:   2022-02-06 16:17:00 -0300
categories: jekyll update
---
<p align="center">
    <img src="/cs-373-spring-2022-geethika-hemkumar/assets/images/geethika_pic.png">
</p>

### What did you do this past week?

This past week, I started attending in-person classes. Both of my CS classes are still online, which is nice in a way because it's easier to see the code or slides being presented. My other three classes are in-person. I've enjoyed the in-person modality, but I will certainly have to get used to the cross-campus walking on Wednesdays as a result of having three back-to-back classes. Besides classes and homework, I held office hours in-person, worked on my research project, and attended leadership meetings for a student organization I am part of. After a very busy first half of the week, a two day break was very much appreciated.

### What's in your way?

I finished my implementation of Collatz this past week, but I still need to finish writing unit/acceptance tests and running my code through the various formatting/testing tools. I feel confident that I'll be able to finish this before Tuesday, but I may need to put quite a bit of work in over the next few days to finish before the deadline. My workload for other classes have started to increase as well, so I'll need to work on figuring out how to manage all of my
assignments amidst my extracurricular activities. 

### What will you do next week?

Next week will be very similar to this past week in terms of attending classes, meetings, and completing assignments. I hope to finish Collatz on Monday or early on Tuesday so I can check that I completed all the requirements before the deadline on Tuesday night. I'll also get started on my data mining assignment. Additionally, I hope to wrap up the coding part of my research project and get started on the second part next week which will be exciting. 

### What did you think of Paper #3: Continuous Integration?

This week's paper was very informative! I've heard of continuous integration, but haven't used it extensively yet. The paper gave a great explanation of what continuous integration is and recommended some useful practices that may be very applicable to the upcoming team project that we'll be working on. One tip that I particularly found useful was using a staged pipeline to facilitate the development process. I also enjoyed learning about diff debugging and will certainly make use of this in the future.  

### What was your experience of UnitTests, Coverage, and IsPrime?

I've used Google Test, a unit test framework for C++, in the past and am somewhat familiar with Python's unittest module. I've yet to write additional unit tests, but while writing my code I've realized some important corner cases that I'll surely use as unit tests. 

I enjoyed learning about and seeing how Coverage worked in class. I've heard about "x% coverage" being associated with unit testing but never fully understood the meaning of this until now.  

The IsPrime exercise was straightforward but still useful, as it allowed me to further practice understanding and fixing code. Additionally, getting a chance to meet and work with others in the class was enjoyable. I was also fascinated by the discussion about optimizations after completing the exercise, as a skill that I'd like to develop further is finding ways to optimize my code. 

### What made you happy this week?

Having Thursday and Friday off this week made me happy! I was able to catch up on some assignments and notably pass all of the Collatz correctness tests. I was also able to get ample sleep and watch more of the show I started a few weeks ago.

### What's your pick-of-the-week or tip-of-the-week?

My tip-of-the-week is to use tmux! It's a terminal multiplexer tool that allows you to open multiple terminals within a single window. Tmux is especially useful when working on a remote machine, as if using tmux you would only need to log in once instead of multiple times in order to gain access to multiple terminal windows. [Here](https://tmuxcheatsheet.com/.) is a website I've been using to reference various tmux commands.
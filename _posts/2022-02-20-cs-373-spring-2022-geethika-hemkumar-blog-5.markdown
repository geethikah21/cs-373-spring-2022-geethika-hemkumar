---
layout: post
title:  "CS373 Spring 2022: Geethika Hemkumar - Blog 5"
date:   2022-02-20 17:00:00 -0300
categories: jekyll update
---
<p align="center">
    <img src="/cs-373-spring-2022-geethika-hemkumar/assets/images/geethika_pic.png">
</p>

### What did you do this past week?

This past week was quite busy for me. I attended classes and meetings and held office hours per usual. I mostly worked on assignments for data mining and world literature. I was happy to find some time this past week to catch up on my literature assignments and figure out a way to stay caught up. Outside of school, I played a lot of wordle and started watching a new TV show.     

### What's in your way?

I've struggled with time management recently. I need to work on figuring out when I'm most productive in a day and capitalize on these times. Additionally, many of my classes/extracurriculars this semester involve group work, so I'll need to figure out when is best for me to schedule meeting times while also setting aside time to complete other homework assignments and study for midterms. 

### What will you do next week?

I'll be continuing my weekly routine of attending classes/meetings and holding office hours. Additionally, I hope to meet with my group to discuss/finalize our website idea and start implementation. I have a midterm this upcoming Wednesday, so I'll be studying for that as well. 

### What did you think of Paper #5: Single Responsibility Principle?

I thought this paper was extremely useful and interesting. The idea of ensuring that each class in an application only has a single responsibility makes sense. The example about the computational geometry and graphical applications in relation to rectangle drawing/area computation clarified the Single Responsibility Principle for me. The diagrams throughout the paper were also very helpful. The SRP is certainly a principle I'll consider when constructing and interconnecting models for my group's project. 

### What was your experience of types and recursion?

I've become familiar with Python types through projects and class assignments but never studied them in-depth. I was previously unaware of the frozenset and deque Python types, so these were espeically interesting to learn about. Overall, I enjoyed learning about various Python data structures and their properties. This knowledge will certainly be useful when deciding which data structure is best for a given task in future projects. 

We didn't get a chance to discuss recursion in too much detail in class after the factorial exercise, but I'm intrigued by the tail recursion function in the class notes and will certainly study this in more detail when I get the time to.  

### What made you happy this week?

My brother's birthday was this weekend, so I came home to celebrate with him! My mom bought a chocolate cake from Central Market that was really good. I always enjoy seeing my family and especially spending time with my brother. I also got a friend to play the wordle this week - it's been fun comparing and discussing our results each day. 

### What's your pick-of-the-week or tip-of-the-week?

My pick-of-the-week is [RapidAPI](https://rapidapi.com/)! It's a website that hosts APIs, a subset of which are free and public, that may be useful for the project in this class and in the future. Using APIs on this website is simple, as you just need to generate an API-specific key via the website and include this in your project using an environment variable. 